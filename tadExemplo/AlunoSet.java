
public interface AlunoSet {

	/* retornar um aluno, adicionar, remover, quantidade e limpar os alunos */
	/* Na interface s� tem a assinatura dos m�todos*/
	public boolean contains (String aluno) ;
	public boolean add (String aluno);
	public boolean remove (String aluno);
	public int size ();
	public void clear();
}
