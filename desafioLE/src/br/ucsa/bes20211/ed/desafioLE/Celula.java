package br.ucsa.bes20211.ed.desafioLE;

/*Crie uma lista com os alunos da turma de Estrutura de dados. Esta lista dever� conter os m�todos:

1) Adicionar os alunos na lista
2) Listar os alunos adicionados na lista*/

public class Celula {

	private Aluno valor;
	private Celula proximo;

	public Aluno getValor() {
		return valor;
	}

	public void setValor(Aluno valor) {
		this.valor = valor;
	}

	public Celula getProximo() {
		return proximo;
	}

	public void setProximo(Celula proximo) {
		this.proximo = proximo;
	}

}
