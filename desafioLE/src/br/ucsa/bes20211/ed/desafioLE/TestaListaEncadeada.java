package br.ucsa.bes20211.ed.desafioLE;
public class TestaListaEncadeada {

	public static void main(String[] args) {

		TestaListaEncadeada testaListaEncadeada = new TestaListaEncadeada();
		ListaEncadeada listaEncadeada = new ListaEncadeada();

		testaListaEncadeada.addAluno(listaEncadeada);
		System.out.println(">> Todos os Alunos foram adicionados com sucesso! << ");
		System.out.println("     Voc� tem os seguintes Alunos adicionados: ");
		System.out.println("");
		while (listaEncadeada.temProximo()) {
			System.out.println(listaEncadeada.getPosicaoAtual().getValor().getNome());
			
		}
	}

	public void addAluno(ListaEncadeada listaEncadeada) {
		Aluno Aluno1 = new Aluno(1, "Robert", 10);
		Aluno Aluno2 = new Aluno(2, "Davi", 20);
		Aluno Aluno3 = new Aluno(3, "Mikaelle", 30);
		Aluno Aluno4 = new Aluno(4, "Samila", 40);
		Aluno Aluno5 = new Aluno(5, "Bruno", 50);
		Aluno Aluno6 = new Aluno(5, "Natalia", 60);

		listaEncadeada.adicionar(Aluno1);
		listaEncadeada.adicionar(Aluno2);
		listaEncadeada.adicionar(Aluno3);
		listaEncadeada.adicionar(Aluno4);
		listaEncadeada.adicionar(Aluno5);
		listaEncadeada.adicionar(Aluno6);
	}

}
