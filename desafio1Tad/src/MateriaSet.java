
/* Implemente um TAD do tipo Disciplina em que contenha as opera��es:
-Consulta
-Inclus�o
-Remo��o
-Retorno da quantidade de professores
-Limpar lista
*/
public interface MateriaSet {

	public boolean consultar(String materia);

	public boolean incluir(String materia);

	public boolean remover(String materia);

	public int disciplinaSize();

	public void disciplinaClear();
}
