
public interface ProfessoresUcsalSet {

	/*
	 * Implemente um TAD do tipo professoresUcsal em que contenha as opera��es:
	 * -Consulta -Inclus�o -Remo��o -Retorno da quantidade de professores -Limpar
	 * lista
	 */
	public boolean consultar(String professoresUcsal);

	public boolean add(String professoresUcsal);

	public boolean remove(String professoresUcsal);

	public int size();

	public void clear();
}
